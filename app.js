const express = require('express');
const bodyParser = require('body-parser');
const csrf = require('csurf');
const cookieParser = require('cookie-parser');
var csrfProtection = csrf({ cookie: true })

const app = express();
const PORT = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(csrf({ cookie: true }));

app.set('view engine', 'ejs');

const authenticateUser = (req, res, next) => {
    const token = req.cookies.authCookie;                                       
    if (token) {
        next();
    } else {
        res.redirect('/login');
    }
  };

app.get('/', authenticateUser, csrfProtection, (req, res) => {
  res.render('form', { csrfToken: req.csrfToken() });
});


app.post('/process', authenticateUser, csrfProtection, (req, res) => {
   

  const amount = req.body.amount;
  const recipient = req.body.recipient;

  res.send(`
    <div style="background-color: #d4edda; color: #155724; border: 1px solid #c3e6cb; padding: 15px; margin-top: 20px; border-radius: 4px;">
      Transfer of ${amount} to ${recipient} successful!
    </div>
  `);
});


app.get('/login', csrfProtection, (req, res) => {
    res.render('login', { csrfToken: req.csrfToken() });
  });
  
app.post('/login', csrfProtection, (req, res) => {
    const { username, password } = req.body;
    if (username === 'user' && password === '123456') {
        res.cookie('authCookie', 'authenticated', { httpOnly: true });
        return res.redirect('/');
    }
    res.send('Invalid credentials. Please try again.');
});


app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});